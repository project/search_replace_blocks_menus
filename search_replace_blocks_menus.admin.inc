<?php

/**
 * @file
 * Admin settings file for search_replace_blocks_menus module.
 */

/**
 * Form constructor for search_replace_blocks_menus_input_form form.
 */
function search_replace_blocks_menus_input_form($form, &$form_state) {
  if (!isset($form_state['values'])) {
    $form_state['values'] = array();
  }
  $scanner = $form_state['values'] + array(
    'wholeword_checkbox' => NULL,
    'search' => NULL,
    'replace' => NULL,
    'block_checkbox' => NULL,
    'menu_checkbox' => 0,
    'caseinsensitive_checkbox' => NULL,
  );
  $form['locations'] = array(
    '#type' => 'container',
    '#prefix' => '<strong>' . t('Search and replace in following locations:') . '</strong>',
    '#suffix' => '<br/>',
  );
  $form['locations']['menu_checkbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Search Or Replace In Menus Title'),
    '#default_value' => $scanner['menu_checkbox'],
  );
  $form['locations']['block_checkbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Search Or Replace In Blocks Body'),
    '#default_value' => $scanner['block_checkbox'],
  );
  $form['wholeword_checkbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Match whole word'),
    '#default_value' => $scanner['wholeword_checkbox'],
  );
  $form['caseinsensitive_checkbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Case-Insensitive Replace'),
    '#default_value' => $scanner['caseinsensitive_checkbox'],
  );
  $form['search'] = array(
    '#type' => 'textfield',
    '#title' => t('Search'),
    '#required' => TRUE,
    '#default_value' => $scanner['search'],
  );
  $form['search_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
    '#submit' => array('search_replace_blocks_menus_form_submit_one'),
  );
  $form['replace'] = array(
    '#type' => 'textfield',
    '#title' => t('Replace'),
    '#required' => FALSE,
    '#default_value' => $scanner['replace'],
  );
  $form['replace_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Replace'),
    '#submit' => array('search_replace_blocks_menus_form_submit_two'),
  );
  return $form;
}

/**
 * Submission handler on Search button.
 */
function search_replace_blocks_menus_form_submit_one($form, &$form_state) {
  // Set search and replace string in session.
  $scanner = $form_state['values'];
  $operator = 'LIKE';
  if ($scanner['wholeword_checkbox']) {
    $word_boundary = '[[:<:]]' . $scanner['search'] . '[[:>:]]';
    $operator = 'REGEXP';
  }
  else {
    $word_boundary = '%' . db_like($scanner['search']) . '%';
  }
  $output = '';
  $options = array();
  // Check search in block checkbox is checked or not.
  if ($scanner['block_checkbox']) {
    $result = db_select('block_custom', 'bc')
              ->fields('bc', array('info', 'bid'))
              ->condition('body', $word_boundary, $operator)
              ->execute()
              ->fetchAll();
    if (!empty($result)) {
      foreach ($result as $value) {
        $output .= l(t('Search String Found in Block @name', array('@name' => $value->info)), 'admin/structure/block/manage/block/' . $value->bid . '/configure', $options);
        $output .= t('<br />');
      }
    }
    else {
      $output .= t('No Match Found For Search String In Blocks.<br />');
    }
  }
  // Check search in menu checkbox is checked or not.
  if ($scanner['menu_checkbox']) {
    $query = db_select('menu_links', 'ml')->fields('ml', array('menu_name', 'mlid'));
    $db_or = db_or();
    $db_or->condition('link_title', $word_boundary, $operator);
    $query->condition($db_or);
    $result = $query->execute()->fetchAll();
    if (!empty($result)) {
      foreach ($result as $value) {
        $output .= l(t('Search String Found in Menu @name', array('@name' => $value->menu_name)), 'admin/structure/menu/item/' . $value->mlid . '/edit', $options);
        $output .= t('<br />');
      }
    }
    else {
      $output .= t('No Match Found For Search String In Menus. <br />');
    }
  }
  drupal_set_message($output, 'status');
  $form_state['rebuild'] = 1;
}

/**
 * Submission handler on Replace button.
 */
function search_replace_blocks_menus_form_submit_two($form, &$form_state) {
  $scanner = $form_state['values'];
  $operator = 'LIKE';
  if ($scanner['wholeword_checkbox']) {
    $word_boundary = '[[:<:]]' . $scanner['search'] . '[[:>:]]';
    $operator = 'REGEXP';
  }
  else {
    $word_boundary = '%' . db_like($scanner['search']) . '%';
  }
  $output = '';
  // Check replace in block checkbox is checked or not.
  if ($scanner['block_checkbox']) {
    $result = db_select('block_custom', 'bc')
              ->fields('bc', array('bid', 'body', 'info'))
              ->condition('body', $word_boundary, $operator)
              ->execute()
              ->fetchAll();
    if (!empty($result)) {
      foreach ($result as $value) {
        if ($scanner['caseinsensitive_checkbox'] && !$scanner['wholeword_checkbox']) {
          $body = str_ireplace($scanner['search'], check_plain($scanner['replace']), $value->body);
        }
        if ($scanner['wholeword_checkbox'] && !$scanner['caseinsensitive_checkbox']) {
          $body = preg_replace('/\b' . $scanner['search'] . '\b/', $scanner['replace'], $value->body);
        }
        if ($scanner['caseinsensitive_checkbox'] && $scanner['wholeword_checkbox']) {
          $pattern = '/\b(' . $scanner['search'] . ')\b/i';
          $body = preg_replace($pattern, check_plain($scanner['replace']), $value->body);
        }
        db_update('block_custom')
          ->fields(array('body' => $body))
          ->condition('bid', $value->bid, '=')
          ->execute();
        $output .= t("Entered String Replaced by @string in block @block.<br />", array('@string' => $scanner['replace'], '@block' => $value->info));
      }
    }
    else {
      $output .= t('No Match Found For Entered String In Blocks.<br />');
    }
  }
  // Check replace in menu checkbox is checked or not.
  if ($scanner['menu_checkbox']) {
    $query = db_select('menu_links', 'ml')
             ->fields('ml', array(
                 'mlid',
                 'link_path',
                 'link_title',
                 'menu_name',
                 )
                 );
    $db_or = db_or();
    $db_or->condition('link_title', $word_boundary, $operator);
    $query->condition($db_or);
    $result = $query->execute()->fetchAll();
    if (!empty($result)) {
      foreach ($result as $value) {
        $replacement = strip_tags($scanner['replace']);
        if ($scanner['caseinsensitive_checkbox']) {
          $value->link_title = strtoupper($value->link_title);
        }
        if ($scanner['wholeword_checkbox']) {
          $title = preg_replace('/\b' . $scanner['search'] . '\b/', check_plain($replacement), $value->link_title);
        }
        else {
          $title = str_replace($scanner['search'], check_plain($replacement), $value->link_title);
        }
        db_update('menu_links')
          ->fields(array('link_title' => $title))
          ->condition('mlid', $value->mlid, '=')
          ->execute();
        $output .= t("Entered String Replaced by @string in menu @menu.<br />", array('@string' => $replacement, '@menu' => $value->menu_name));
      }
    }
    else {
      $output .= t('No Match Found For Entered String In Menus.<br />');
    }
  }
  drupal_set_message($output, 'status');
  $form_state['rebuild'] = 1;
}


/**
 * Validate form.
 */
function search_replace_blocks_menus_input_form_validate($form, &$form_state) {
  $scanner = $form_state['values'];
  if ($scanner['replace'] == '' && $form_state['submit_handlers'][0] == 'search_replace_blocks_menus_form_submit_two') {
    form_set_error('replace', t('Please enter some keywords In Replace field.'));
    return;
  }
  if ($scanner['menu_checkbox'] == 0 && $scanner['block_checkbox'] == 0) {
    form_set_error('menu_checkbox', t('Please select atleast one search or replace location.'));
    return;
  }
}
