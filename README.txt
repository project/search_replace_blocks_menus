------------------------
MODULE STRUCTURE
------------------------

This module Search and replace a string in all 
custom added block body and menus title.

------------------------
REQUIREMENTS
------------------------

1. search_replace_blocks_menus : This module requires a supported version 
   of Drupal.

------------------------
INSTALLATION
------------------------
1. Copy the whole "search_replace_blocks_menus" directory to your modules
  directory - "sites/all/modules"
2. Enable the module.
3. Set your desired permissions at - "admin/people/permissions".

------------------------
USAGE
------------------------
 1. Go to "/admin/config/system/replace-blocks-menus".
 2. Fill required fields in form.

---------------------
Permissions available
---------------------
1. Access replace Blocks Menus Form
